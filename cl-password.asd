(asdf:defsystem #:cl-password
  :serial t
  :depends-on (#:ironclad)
  :components ((:file "packages")
               (:file "cl-password")))
