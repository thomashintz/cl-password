(require 'ironclad)
(cl:defpackage :cl-password
  (:use #:cl #:ironclad)
  (:shadow :null)
  (:export :make-random-salt
           :unsupported-hash-error
           :hash-password
           :check-password))
